package ru.t1.sarychevv.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.api.repository.IProjectRepository;
import ru.t1.sarychevv.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public void create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        add(project);
    }

    public void create(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        add(project);
    }

}
