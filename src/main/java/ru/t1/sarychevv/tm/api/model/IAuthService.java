package ru.t1.sarychevv.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@NotNull String login, @NotNull String password, @NotNull String email);

    void login(@NotNull String login, @NotNull String password);

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId();

    @NotNull
    User getUser();

    void checkRoles(@Nullable Role[] roles);

}
